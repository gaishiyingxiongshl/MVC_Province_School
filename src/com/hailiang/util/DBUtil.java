package com.hailiang.util;


import org.apache.commons.dbcp.BasicDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;


public class DBUtil {
    private static Properties ps = new Properties();
    private static DataSource ds ;
   static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static Connection getCon() throws Exception {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("dbcp.properties");
        ps.load(is);
        System.out.println(ps.get("url"));
        ds = BasicDataSourceFactory.createDataSource(ps);
        Connection conn = ds.getConnection();
        return conn;
    }

}
