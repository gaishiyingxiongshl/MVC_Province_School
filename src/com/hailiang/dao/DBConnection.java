package com.hailiang.dao;

import com.hailiang.domain.ProvinceBean;
import com.hailiang.util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DBConnection {
    public List getProvince() throws Exception {
        Connection con = DBUtil.getCon();
        PreparedStatement ps = con.prepareStatement("select * from province");
        ResultSet rs = ps.executeQuery();
        List list = new ArrayList();
        while (rs.next()){
            ProvinceBean pb = new ProvinceBean();
            pb.setId(rs.getInt(1));
            pb.setName(rs.getString(2));
            list.add(pb);
        }
        System.out.println(list);
    return list;
    }
}
