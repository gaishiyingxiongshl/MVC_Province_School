package com.hailiang.contorller;

import com.alibaba.fastjson.JSON;
import com.hailiang.dao.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class MyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DBConnection dbConnection = new DBConnection();
        try {
            List province = dbConnection.getProvince();
            String s = JSON.toJSONString(province);
            resp.getWriter().write(s);
        } catch (Exception e) {
            e.printStackTrace();
        }




    }
}